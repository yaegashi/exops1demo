[CmdletBinding()]
Param(
    [string]$TokenUri
)

Set-StrictMode -Version 3
$ErrorActionPreference = "Stop"

$AppId = "a0c73c16-a7e3-4564-9a95-2bdf47383716"
$Scopes = @("https://outlook.office365.com/.default", "offline_access")
$RemoteConfig = "Microsoft.Exchange"
$RemoteUri = "https://ps.outlook.com/powershell-liveid?BasicAuthToOAuthConversion=true"

$token = $null
$save = $false

if ($TokenUri) {
    try {
        Write-Verbose "Load the token from Azure Storage Blob"
        $token = Invoke-RestMethod -Uri $TokenUri
    }
    catch {
        Write-Verbose "Failed to load: $_"
        $token = $null
    }
}

if ($null -ne $token) {
    try {
        $now = Get-Date
        $next = $token.ExpiresOn.AddMinutes(-10)
        Write-Verbose "Current time: $now"
        Write-Verbose "Next refresh: $next"
        if ($next.CompareTo($now) -lt 0) {
            Write-Verbose "Refresh the token"
            $token = New-PartnerAccessToken -ApplicationId $AppId -Scopes $Scopes -RefreshToken $token.RefreshToken
            $save = $true
        }
    }
    catch {
        Write-Warning "Failed to refresh: $_"
        $token = $null
    }
}

if ($null -eq $token) {
    try {
        Write-Verbose "Authenticate and create a new token"
        $token = New-PartnerAccessToken -ApplicationId $AppId -Scopes $Scopes -UseDeviceAuthentication
        $save = $true
    }
    catch {
        Write-Warning "Failed to authenticate: $_"
        $token = $null
    }
}

if ($null -eq $token) {
    Write-Error "No authorization token"
}

if ($save -and $TokenUri) {
    try {
        Write-Verbose "Save the token into Azure Storage Blob"
        $token | ConvertTo-Json | Invoke-RestMethod -Uri $TokenUri -Method PUT -ContentType application/json -Headers @{"x-ms-blob-type" = "BlockBlob" }
    }
    catch {
        Write-Warning "Failed to save: $_"
    }
}

# Connect to the Exchange Online PowerShell server and import the session
$pass = ConvertTo-SecureString "Bearer $($token.AccessToken)" -AsPlainText -Force
$cred = New-Object System.Management.Automation.PSCredential($token.Account.Username, $pass)
$session = New-PSSession -ConfigurationName $RemoteConfig -ConnectionUri $RemoteUri -Credential $cred -Authentication Basic -AllowRedirection
Import-PSSession -AllowClobber -Session $session

# Exchange Online PowerShell cmdlets are available from this point onward
Get-Mailbox | Format-Table
