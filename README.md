# Exchange Online PowerShell Automation Demo

## Introduction

[ExchangeOnlineAutomate.ps1](ExchangeOnlineAutomate.ps1)
demonstrates
[Exchange Online PowerShell remoting](https://docs.microsoft.com/en-us/powershell/exchange/exchange-online/exchange-online-powershell)
with
[PartnerCenter module](https://github.com/microsoft/Partner-Center-PowerShell)
for authentication.

It has no dependency on non-portable authentication modules like AzureAD
but supports
[the OAuth2 device auth grant flow](https://docs.microsoft.com/en-us/azure/active-directory/develop/v2-oauth2-device-code).
This means you can run it portablly on Windows/macOS/Linux
using modern and safer authentication method.

It also supports persisting OAuth2 tokens in Azure Blob Storage,
which enables automating Exchange Online administration
on common CI/CD infrastructure without any user intervention.

## Persistent token storage

You need to create your own blob URI with SAS for the persistent token storage.
It's strongly recommended to [use a stored access policy](https://docs.microsoft.com/en-us/rest/api/storageservices/define-stored-access-policy) in case that revocation is required.
The following exapmle shows how to create an access policy and generate a URI with SAS using Azure CLI:

```console
$ az storage container policy create --account-name l0wstorage --account-key xxxxxxxx --container-name tokens --name exops1demo-policy --permission crw --expiry 2100-01-01T00:00:00Z
$ az storage blob generate-sas --account-name l0wstorage --account-key xxxxxxxx --container-name tokens --name exops1demo.json --policy-name exops1demo-policy --full-uri -o tsv
https://l0wstorage.blob.core.windows.net/tokens/exops1demo.json?sr=b&si=exops1demo-policy&sig=f6XvzAkrbaRhcPZgW6SOGNhWffSHSuqFYJMjD9AoRz0%3D&sv=2018-11-09
```

When the URI above passed to `-TokenUri` parameter,
the script pauses to prompt you for the authentication with a device code on the first run:

    WARNING: To sign in, use a web browser to open the page https://microsoft.com/devicelogin and enter the code B6UD2PR7H to authenticate.

After successful sign-in as an Exchange Online user,
the script resumes and uploads acquired tokens in JSON format to the URI.
It won't ask you for authentication any more on subsequent invocations.

## GitLab CI configuraion example

[.gitlab-ci.yml](.gitlab-ci.yml) shows an example usage
with PowerShell Core 7.0 in Ubuntu 18.04 container on GitLab CI.
The URI with SAS is set as `$TokenUri` CI/CD variable.
You can see how it actually works in the following jobs:

- 1st run: https://gitlab.com/yaegashi/exops1demo/-/jobs/526931295
- 2nd run: https://gitlab.com/yaegashi/exops1demo/-/jobs/526931675

**WARNING: You shouldn't run this kind of CI in public projects.**
Sensitive information such as a URI with SAS
may accidentally be exposed in a build logs as shown above.
In such cases you should immediately re-generate a new URI with SAS
after renewing the access policy or the storage account key.